# Phishing Flooder

Use this to flood a scammers phishing website with a shitload of useless emails and passwords.

## Instructions:

1. Open your browsers developer tools.

2. Select "Network" and check "Preserve Logs".

![Image1](https://i.imgur.com/ViY8bUb.png)

3. Enter fake information into the phishing page.

4. Look through results to find the request url, username/email, and password fields.

![Image2](https://i.imgur.com/wKnALEg.png)


5. Fill in the script with the phishing page's information.

6. Run the script and watch the flooding.

![Image3](https://i.imgur.com/Kdbjluy.png)

7. Repeat until satified.