import requests
import random
import string
import json

# Enter the Request URL

requestUrl = 'http://www.medspaarizona.com/def/toda/webmail.php?email=email&password=password&logintype=webmail&submit_btn=Go+to+step+2'

# Enter the email/username field name

queryStringOne = 'email'

# Enter the password field name

passwordField = 'password'

# Opens the JSON files that contain the names, passwords, and domains

names = json.loads(open('names.json').read())
passwords = json.loads(open('passwords.json').read())
domain = json.loads(open('domains.json').read())

for name in names:
    # Chooses random numbers to add to email/username to make it look real

    name_extra = ''.join(random.choice(string.digits))

    # Picks random name and adds a number, then selects random domain

    username = name.lower() + name_extra + random.choice(domain)

    # Chooses random password from passwords.json

    password = random.choice(passwords)

    # Sends the fake data to the scammer

    requests.post(requestUrl, allow_redirects=False, data={
        queryStringOne: username,
        passwordField: password
    })

    # Prints and output of the generated emails and passwords

    print('Fake email --> %s |---| Fake password --> %s' % (username, password))
